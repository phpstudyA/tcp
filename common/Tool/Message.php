<?php

namespace common\Tool;

class Message
{
    /**
     * @param string $message
     * @return string
     */
    public static function assembleMessage(string $message): string
    {
        return pack('N', strlen($message)) . pack('a*', $message);
    }
}