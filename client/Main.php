<?php

namespace client;

require '../common/Tool/Message.php';

use Co\Client;
use common\Tool\Message;
use Swoole\Event;
use Swoole\Process\Pool;
use Swoole\Runtime;

class Main
{
    public function __construct()
    {
        $pool = new Pool(1);
        // 让每个OnWorkerStart回调都自动创建一个协程
        $pool->set(['enable_coroutine' => true]);
        // 让全部协程都变成异步IO
        Runtime::enableCoroutine();
        $pool->on('workerStart', function (Pool $pool, $id) {
            $client = new Client(SWOOLE_SOCK_TCP);
            $client->set([
                'open_length_check'     => true,
                'package_max_length'    => 1024 * 1024,
                'package_length_type'   => 'N',
                'package_length_offset' => 0,
                'package_body_offset'   => 4
            ]);

            if (!$client->connect('127.0.0.1', '9555')) {
                echo $client->errMsg;
                return;
            }

            defer(function () use ($client) {
                $client->close();
            });

            go(function () use ($client) {
                while (true) {
                    $data = $client->recv(-1);
                    if ($data === false || $data === '') {
                        echo 'exit：err' . $client->errCode . PHP_EOL;
                        break;
                    }

                    $data = unpack('Nlen/a*data', $data);
                    if (empty($data) || !isset($data['data']) || $data['data'] === '') {
                        continue;
                    }

                    $data = $data['data'];
                    echo $data . PHP_EOL;
                }
            });

            while (true) {
                $client->send(Message::assembleMessage(trim(fgets(STDIN))));
                if ($client->errCode !== 0) {
                    echo '服务器关闭，即将退出' . PHP_EOL;
                    // 关闭
                    $pool->shutdown();
                }
            }
        });

        $pool->start();

        Event::wait();
    }

    final protected function debug()
    {
        echo "test" . PHP_EOL;
    }
}

new Main();