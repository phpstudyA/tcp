<?php

namespace server;

require '../common/Tool/Message.php';

use Co\Server;
use common\Tool\Message;
use Swoole\Coroutine\Server\Connection;
use Swoole\Event;
use Swoole\Process;
use Swoole\Process\Pool;
use Swoole\Runtime;

class Main
{
    /**
     * @var array
     */
    private array  $conns;
    private string $message;

    public function __construct()
    {
        // 初始化
        $this->conns   = [];
        $this->message = '';

        $pool = new Pool(1);
        // 让每个OnWorkerStart回调都自动创建一个协程
        $pool->set(['enable_coroutine' => true]);
        // 让全部都变成异步IO
        Runtime::enableCoroutine();
        $pool->on('workerStart', function ($pool, $id) {
            //每个进程都监听9501端口
            $server = new Server('0.0.0.0', '9555');

            //收到15信号关闭服务
            Process::signal(SIGTERM, function () use ($server) {
                $server->shutdown();
            });

            go(function () use ($server) {
                $server->set([
                    'open_length_check'     => true,
                    'package_max_length'    => 1024 * 1024,
                    'package_length_type'   => 'N',
                    'package_length_offset' => 0,
                    'package_body_offset'   => 4,
                ]);
                $server->handle(function (Connection $conn) {
                    $connCount     = count($this->conns);
                    $this->conns[] = $conn;

                    \Co\defer(function () use ($conn) {
                        $conn->close();
                    });

                    // 发送欢迎消息 及 监听上线用户
                    go(function () use ($conn, $connCount) {
                        $this
                            ->setMessage('欢迎！')
                            ->send($conn, $connCount);

                        foreach ($this->conns as $connKey => $foreachConnItem) {
                            $socket = $conn->exportSocket();

                            if ($foreachConnItem->exportSocket()->fd === $socket->fd) {
                                continue;
                            }

                            $ipInfo = $socket->getpeername();
                            $this
                                ->setMessage('用户IP：' . $ipInfo['address'] . ':' . $ipInfo['port'] . '上线了，当前用户量：' . ($connCount + 1))
                                ->send($foreachConnItem, $connKey);
                        }
                    });

                    // 循环获取消息
                    while (true) {
                        $data = $conn->recv(-1);
                        if ($data === false || $data === '') {
                            break;
                        }

                        $data = unpack('Nlen/a*data', $data);
                        if (empty($data) || !isset($data['data']) || $data['data'] === '') {
                            continue;
                        }

                        $data = $data['data'];
                        // 收到当前用户的消息，转发给其他用户
                        go(function () use ($conn, $data) {
                            foreach ($this->conns as $connKey => $foreachConnItem) {
                                $socket = $conn->exportSocket();

                                if ($foreachConnItem->exportSocket()->fd === $socket->fd) {
                                    continue;
                                }

                                $ipInfo = $socket->getpeername();
                                $this
                                    ->setMessage("IP地址" . $ipInfo['address'] . ':' . $ipInfo['port'] . '的用户发送消息：' . $data)
                                    ->send($foreachConnItem, $connKey);
                            }
                        });
                    }
                });
                $server->start();
            });
        });

        $pool->start();

        Event::wait();
    }

    /**
     * @param Connection $toConn
     * @param int $toKey
     * @return void
     */
    public function send(Connection $toConn, int $toKey)
    {
        if ($this->message === '') {
            return;
        }

        $res           = $toConn->send(Message::assembleMessage($this->message));
        $this->message = '';
        // 发现下线的用户，关闭连接并且通知其他用户
        if ($res === false) {
            $toConn->close();

            unset($this->conns[$toKey]);

            // 发送下线消息
            foreach ($this->conns as $key => $conn) {
                $this
                    ->setMessage('有用户下线了，当前用户量' . count($this->conns))
                    ->send($conn, $key);
            }
        }
    }

    /**
     * @param string $message
     * @return self
     */
    private function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }
}

new Main();